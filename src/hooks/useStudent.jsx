import { useCallback, useMemo, useState } from "react";
import { groups } from "../data/groups";
import { v4 as uuidv4 } from "uuid";

export const useStudent = () => {
  const [validated, setValidated] = useState(false);
  const [student, setStudent] = useState({
    product_name: "",
    price: "", 
    group: groups[0],
    quantity: "", 
    description: "",
  });
  const studentsJson = JSON.parse(localStorage.getItem("students")) || [];
  const [students, setProducts] = useState(studentsJson);
  const setStudents = useCallback((newProducts) => {
    setProducts(newProducts);
    localStorage.setItem("students", JSON.stringify(newProducts));
  },[students]);
  const [search, setSearch] = useState("");
  const [selected, setSelected] = useState(null);

  const [changeCategory, setChangeCategory] = useState("all");
  const [sPriceValue, setSPriceValue] = useState("increase");

  let StudentsE = students;
  let first, second, eStudent;

  if (sPriceValue === "increase") {
    for (let i = 0; i < StudentsE.length; i++) {
      first = StudentsE[i];
      for (let j = 0; j < StudentsE.length; j++) {
        second = StudentsE[j];
        if (+first.price < +second.price) {
          eStudent = StudentsE[i];
          StudentsE[i] = StudentsE[j];
          StudentsE[j] = eStudent;
        }
      }
    }
  } else {
    for (let i = 0; i < students.length; i++) {
      first = StudentsE[i];
      for (let j = 0; j < StudentsE.length; j++) {
        second = StudentsE[j];
        if (+first.price > +second.price) {
          eStudent = StudentsE[i];
          StudentsE[i] = StudentsE[j];
          StudentsE[j] = eStudent;
        }
      }
    }
  }

   let results;
  if (changeCategory !== "all") {
    results = StudentsE
      .filter((student) => student.group === changeCategory)
      .filter(
        (student) =>
          student.product_name.toLowerCase().includes(search) ||
          student.group.toLowerCase().includes(search)
      );
  } else {
    results = StudentsE.filter(
      (student) =>
        student.product_name.toLowerCase().includes(search) ||
        student.group.toLowerCase().includes(search)
    );
  }



  const handleChange = useCallback(
    (e) => {
      setStudent({ ...student, [e.target.name]: e.target.value });
    },
    [student]
  );

  const submit = useCallback(
    (e) => {
      e.preventDefault();
      setValidated(true);
      const form = e.currentTarget;
      if (form.checkValidity()) {
        let newStudents;
        if (selected === null) {
          newStudents = [...students, student];
        } else {
          newStudents = students.map((el) => {
            if (el.id === selected) {
              return student;
            } else {
              return el;
            }
          });
          setSelected(null);
        }
        setStudents(newStudents);
        setStudent({
          product_name: "",
          price: "", 
          group: groups[0],
          quantity: "", 
          description: "",
          id: uuidv4(),
        });
        setValidated(false);
      }
    },
    [student, students, selected]
  );


  const editStudent = useCallback(
    (id) => {
      let newStudents = students.find((student) => student.id === id);
      setSelected(id);
      setStudent(newStudents);
    },
    [students]
  );
  

   const delet = useCallback(
    (id) => {
      let newStudents;
      let deleteRequest = window.confirm("Uchirilsinmi");
      if (deleteRequest) {
        newStudents = students.filter((student) => student.id !== id);
        setStudents(newStudents);
      }
    },
    [students]
  );

  const handleSearch = useCallback((e) => {
    setSearch(e.target.value.trim().toLowerCase());
  }, []);

  const getAverageAge = (students) => {
    console.log("getAverageAge");
    return (
      students.reduce((acc, student) => acc + +student.price, 0) / students.length
    );
  };

  const averageAge = useMemo(() => getAverageAge(students), [students]);
  
  
  const gPriceValue = (e) => {
    console.log(e.target.value);
    setSPriceValue(e.target.value);
  };

   const filterCategory = (e) => {
    console.log(e.target.value);
    setChangeCategory(e.target.value);
  };


  return {
    validated,
    student,
    StudentsE,
    results,
    search,
    averageAge,
    selected,
    handleSearch,
    handleChange,
    gPriceValue,
    sPriceValue,
    filterCategory,
    changeCategory,
    submit,
    editStudent,
    delet,
  };
};
