import { useState } from 'react';
import './login.css'


function LoginP() {
  const [user, setUser] = useState({ username: "", password: "" });

  const login = () => {
    if (user.username === "samariddin" && user.password === "12345") {
      localStorage.setItem("isAuth", '1');
      window.location = "/";
    } else {
      alert("Check info !");
    }
  }
  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  return (
    <div className='abody'>
      <div className="blob"></div>
      <div className="wrapper">
          <form action=''>
              <h2>Login</h2>
              <div className="input-box">
                  <input className='login__email' type="text" name="username" value={user.username} onChange={handleChange} required/>
                  <label>Username</label>
              </div>
              <div className="input-box">
                  <input className='login__password' type="password" name="password" value={user.password} onChange={handleChange} required/>
                  <label>Password</label>
              </div>
              <div className="remember-forgot">
                  <label for="Chek"><input id="Chek" type="checkbox"/>Remember me </label>
                  <p style={{cursor: "pointer", borderBottom: "1px solid white"}}>Forgot Password?</p>
              </div>
              <div className="button login__btn" onClick={login}>
                <span className="btn-text" user-selcect="none">Login</span>
              </div>
              <div className="register-link">
                  <p>Don't have a account? <span style={{cursor: "pointer", borderBottom: "1px solid white"}}>Sign up</span></p>
              </div>
          </form>
      </div>
    </div>
  )
}

export default LoginP