import { memo } from "react";
import { Button, Form, FormGroup } from "react-bootstrap";
import PropTypes from "prop-types";
import { groups } from "../data/groups";

const StudentForm = memo(
  ({ student, StudentsE, handleChange, submit, validated, selected }) => {
    const { product_name, price, group, quantity, description } = student;
    return (
      <Form noValidate onSubmit={submit} validated={validated}>
        <FormGroup className="mt-3">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            required
            onChange={handleChange}
            value={product_name}
            name="product_name"
            placeholder="Product Name"
            type="text"
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          <Form.Control.Feedback type="invalid">
            Please fill!
          </Form.Control.Feedback>
        </FormGroup>
        <FormGroup className="mt-3">
          <Form.Label>Price</Form.Label>
          <Form.Control
            required
            onChange={handleChange}
            value={price}
            name="price"
            placeholder="Price"
            type="number"
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          <Form.Control.Feedback type="invalid">
            Please fill!
          </Form.Control.Feedback>
        </FormGroup>
        <FormGroup className="mt-3">
          <Form.Label>Category</Form.Label>
          <Form.Select onChange={handleChange} value={group} name="group">
            {groups.map((gr) => (
              <option key={gr} value={gr}>
                {gr}
              </option>
            ))}
          </Form.Select>
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          <Form.Control.Feedback type="invalid">
            Please fill!
          </Form.Control.Feedback>
        </FormGroup>

        <FormGroup className="mt-3">
          <Form.Label>Quantity</Form.Label>
          <Form.Control
            required
            onChange={handleChange}
            value={quantity}
            name="quantity"
            placeholder="Quantity"
            type="number"
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          <Form.Control.Feedback type="invalid">
            Please fill!
          </Form.Control.Feedback>
        </FormGroup>
        <FormGroup className="mt-3">
          <label>Description</label>
          <textarea required onChange={handleChange} value={description} name="description" className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
          <Form.Control.Feedback type="invalid">
            Please fill!
          </Form.Control.Feedback>
        </FormGroup>
        <Button className="mt-3" type="submit">
          {selected ? "Save" : "Add"}
        </Button>
      </Form>
    );
  }
);

StudentForm.displayName = StudentForm;

StudentForm.propTypes = {
  student: PropTypes.object,
  StudentsE: PropTypes.object,
  handleChange: PropTypes.func,
  submit: PropTypes.func,
  validated: PropTypes.bool,
  selected: PropTypes.bool,
};

export default StudentForm;
