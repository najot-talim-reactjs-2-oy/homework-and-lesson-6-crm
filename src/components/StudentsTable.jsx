import { memo } from "react";
import { Table } from "react-bootstrap";
import PropTypes from "prop-types";

const StudentsTable = memo(({ students, editStudent, delet}) => {
  return (
    <Table striped bordered hover style={{fontSize: "15px"}}> 
      <thead>
        <tr>
          <th>No</th>
          <th>Product Name</th>
          <th>Price</th>
          <th>Category</th>
          <th>Quantity</th>
          <th>Description</th>
          <th className="text-end">Actions</th>
        </tr>
      </thead>
      <tbody>
        {students.map(({  product_name, price, group, quantity, description, id }, i) => (
          <tr key={i}>
            <td>{i + 1}</td>
            <td>{product_name}</td>
            <td>{price}</td>
            <td>{group}</td>
            <td>{quantity}</td>
            <td>{description}</td>
            <td className="text-end">
              <button
                className="btn btn-primary me-3"
                onClick={() => editStudent(id)}
                style={{fontSize: "12px"}}
              >
                Edit
              </button>
              <button style={{fontSize: "12px"}} className="btn btn-danger" onClick={() => delet(id)}>Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
});

StudentsTable.propTypes = {
  students: PropTypes.array,
  editStudent: PropTypes.func,
  delet: PropTypes.func,
};

StudentsTable.displayName = "StudentsTable";

export default StudentsTable;
