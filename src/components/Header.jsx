import { memo } from "react";
import PropTypes from "prop-types";
import { Form, InputGroup } from "react-bootstrap";
import { groups } from "../data/groups";

const Header = memo(({ search, handleSearch, averageAge, sPriceValue, gPriceValue, changeCategory, filterCategory }) => {
  return (
    <Form>
      <InputGroup className="my-3">
        <Form.Control
          name="search"
          value={search}
          onChange={handleSearch}
          placeholder="Searching"
          aria-label="Searching"
          aria-describedby="basic-addon2"
        />
        <InputGroup.Text>{averageAge}</InputGroup.Text>
         <Form.Group>
         <select value={sPriceValue} onChange={gPriceValue} className="form-select" name="sort">
            <option value="increase" name="increase">Increase</option>
            <option value="decrease">Decrease</option>
          </select>
         </Form.Group>
         <Form.Group>
          <select 
            onChange={filterCategory}
            value={changeCategory}
            name="group"
            id="group"
            className="form-select">
            <option className="font-medium" value="all">
              Category filter
            </option>
            <option className="font-medium" value="all">
              All
            </option>
            {groups.map((gr) => (
              <option key={gr} value={gr}>
                {gr}
              </option>
            ))}
          </select>
         </Form.Group>
      </InputGroup>
    </Form>
  );
});

Header.propTypes = {
  search: PropTypes.string,
  handleSearch: PropTypes.func,
  averageAge: PropTypes.number,
};

Header.displayName = "Header";

export default Header;
